EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L climate:I2C_STANDARDQWIIC J2
U 1 1 61970864
P 5100 5190
F 0 "J2" H 5058 5800 45  0000 C CNN
F 1 "QWIIC" H 5058 5716 45  0000 C CNN
F 2 "climate:1X04_1MM_RA" H 5100 5690 20  0001 C CNN
F 3 "" H 5100 5190 50  0001 C CNN
F 4 "CONN-13729" H 5058 5621 60  0000 C CNN "Field4"
	1    5100 5190
	1    0    0    -1  
$EndComp
Text Label 5330 4890 0    50   ~ 0
SCL
Text Label 5330 4990 0    50   ~ 0
SDA
Text Label 5330 5090 0    50   ~ 0
VCC
Text Label 5330 5190 0    50   ~ 0
GND
Wire Wire Line
	5200 4890 5330 4890
Wire Wire Line
	5200 4990 5330 4990
Wire Wire Line
	5330 5090 5200 5090
Wire Wire Line
	5330 5190 5200 5190
Text Label 4840 2740 2    50   ~ 0
SDA
Text Label 4840 2840 2    50   ~ 0
GND
Text Label 4840 2940 2    50   ~ 0
VCC
Text Label 4840 3040 2    50   ~ 0
SCL
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 6197468A
P 5150 2840
F 0 "J1" H 5230 2832 50  0000 L CNN
F 1 "HYT271" H 5230 2741 50  0000 L CNN
F 2 "climate:EdgeCard_4" H 5150 2840 50  0001 C CNN
F 3 "~" H 5150 2840 50  0001 C CNN
	1    5150 2840
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2740 4840 2740
Wire Wire Line
	4950 2840 4840 2840
Wire Wire Line
	4840 2940 4950 2940
Wire Wire Line
	4950 3040 4840 3040
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 6197A65D
P 5260 6480
F 0 "J4" H 5340 6522 50  0000 L CNN
F 1 "Conn_01x01" H 5340 6431 50  0000 L CNN
F 2 "climate:BigPadB" H 5260 6480 50  0001 C CNN
F 3 "~" H 5260 6480 50  0001 C CNN
	1    5260 6480
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 6197A9CB
P 4700 6480
F 0 "J3" H 4618 6255 50  0000 C CNN
F 1 "Conn_01x01" H 4618 6346 50  0000 C CNN
F 2 "climate:BigPad" H 4700 6480 50  0001 C CNN
F 3 "~" H 4700 6480 50  0001 C CNN
	1    4700 6480
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 6480 5060 6480
$EndSCHEMATC
